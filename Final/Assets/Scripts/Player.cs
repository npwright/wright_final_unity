﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [System.Serializable]
    public class PlayerStats {
        public int maxHealth = 100;
        public int maxO2 = 100;

        private int _curHealth;
        public int curHealth
       {
			get { return _curHealth; }
            set { _curHealth = Mathf.Clamp(value, 0, maxHealth); }
		}

        private float _curO2;
        public float curO2
        {
            get { return _curO2; }
            set { _curO2 = Mathf.Clamp(value, 0, maxO2); }
        }


        public void Init()
        {
            curHealth = maxHealth;
            curO2 = maxO2;
        }

    }

    public PlayerStats stats = new PlayerStats();
    public int fallBoundry = -20;

    public string deathSoundName = "DeathVoice";
    public string damageSoundName = "Grunt";

    private AudioManager audioManager;

    [SerializeField]
    private StatusIndicator statusIndicator;

    private void Start()
    {

        audioManager = AudioManager.instance;
        stats.Init();

        if (statusIndicator == null)
        {
            Debug.LogError("No status indicator referenced on Player");
        }
        else
        {
            statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
        }
    }

    private void Update()
    {
        Oxygen();
        
        if(transform.position.y <= fallBoundry)
        {
            DamagePlayer (9999999);
        }
    }


    private void Oxygen()
    {
        stats.curO2 -= Time.deltaTime;
        print(stats.curO2);
        if (stats.curO2 <= 0)
        {
            audioManager.PlaySound(deathSoundName);
            GameMaster.KillPlayer(this);
        }

        statusIndicator.SetO2(stats.curO2, stats.maxO2);
    }


    public void DamagePlayer(int damage) {
        stats.curHealth -= damage;
        if (stats.curHealth <= 0)
        {
            audioManager.PlaySound(deathSoundName);
            GameMaster.KillPlayer(this);
        }
        else
        {
            audioManager.PlaySound(damageSoundName);
        }

        statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
    }

}
