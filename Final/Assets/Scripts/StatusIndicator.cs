using UnityEngine;
using UnityEngine.UI;

public class StatusIndicator : MonoBehaviour {

	public RectTransform healthBarRect;
	public Text healthText;
    public RectTransform o2BarRect;
    public Text o2Text;

/*	void Start()
	{
		if (healthBarRect == null)
		{
			Debug.LogError("STATUS INDICATOR: No health bar object referenced!");
		}
		if (healthText == null)
		{
			Debug.LogError("STATUS INDICATOR: No health text object referenced!");
		}

        if (o2BarRect == null)
        {
            Debug.LogError("STATUS INDICATOR: No o2 bar object referenced!");
        }
        if (o2Text == null)
        {
            Debug.LogError("STATUS INDICATOR: No o2 text object referenced!");
        }

    } */

	public void SetHealth(int _cur, int _max)
	{
		float _value = (float)_cur / _max;

		healthBarRect.localScale = new Vector3(_value, healthBarRect.localScale.y, healthBarRect.localScale.z);
		healthText.text = _cur + "/" + _max + " HP";
	}

    public void SetO2(float _cur, float _max)
    {
        float _value = _cur / _max;

        o2BarRect.localScale = new Vector3(_value, o2BarRect.localScale.y, o2BarRect.localScale.z);
        o2Text.text =(int)(_cur) + "/" + (int)(_max) + "O2";
    }



}
